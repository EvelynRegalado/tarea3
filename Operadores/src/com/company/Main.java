package com.company;

public class Main {

    public static void main(String[] args) {
        //OPERADORES ARITMETICOS (+, -, *, /, %, )

        Operadores ope = new Operadores();
        System.out.println("OPERADORES ARITMETICOS");
        System.out.println("La suma es: " + ope.respuestaS);
        System.out.println("La resta es: " + ope.respuestaR);
        System.out.println("La multiplicacion es: " + ope.respuestaM);
        System.out.println("La división es: " + ope.respuestaD);
        System.out.println("El resto de la divisió es: " + ope.respuestaRD);

        //OPERADORES RELACIONALES (<, >, <=, >=, !=, ==)
        System.out.println("OPERADORES RELACIONALES");
        System.out.println("Mayor que: " + ope.respuestaMayq);
        System.out.println("Menor que: " + ope.respuestaMenq);
        System.out.println("Menor igual que: " + ope.respuestaMenI);
        System.out.println("Mayor o igual que: " + ope.respuestaMayI);
        System.out.println("Distinto: " + ope.respuestaDis);
        System.out.println("Igual: " + ope.respuestaIgual);

        //OPERADORES LOGICOS (&&, ||, !)
        System.out.println("OPERADORES LOGICOS");
        System.out.println("Respuesta 1" + ope.resp1);
        System.out.println("Respuesta 2" + ope.resp2);
        System.out.println("Respuesta 3" + ope.resp3);
        System.out.println("Respuesta 4" + ope.resp4);
        System.out.println("Respuesta 5" + ope.resp5);
        System.out.println("Respuesta 6" + ope.resp6);
        System.out.println("Respuesta 7" + ope.resp7);

        //OPERADORES ASIGNACION
        System.out.println("OPERADORES ASIGNACION");
        System.out.println("Respuesta Asignacion 1" + ope.respAsig1);
        System.out.println("Respuesta Asignacion 2" + ope.respAsig2);
        System.out.println("Respuesta Asignacion 3" + ope.respAsig3);
        System.out.println("Respuesta Asignacion 4" + ope.respAsig4);
        System.out.println("Respuesta Asignacion 5" + ope.respAsig5);
        System.out.println("Respuesta Asignacion 6" + ope.respAsig6);
    }
}
