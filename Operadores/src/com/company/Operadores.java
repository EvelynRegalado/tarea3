package com.company;

/**
 * Created by Pc Evelyn on 04/06/2017.
 */
public class Operadores {
    //OPERADORES ARITMETICOS (+, -, *, /, %, )

    int a=10;
    int b=3;
    int respuestaS=a+b;
    int respuestaR=a-b;
    int respuestaM=a*b;
    double respuestaD=a/b;
    int respuestaRD=a%b;

    //OPERADORES RELACIONALES (<, >, <=, >=, !=, ==)
    int c=7;
    int d=9;
    int e=7;

    boolean respuestaMayq= c>d;
    boolean respuestaMenq= c<d;
    boolean respuestaMenI= e<=d;
    boolean respuestaMayI= c>=d;
    boolean respuestaDis= c!=e;
    boolean respuestaIgual= c==e;

    //OPERADORES LOGICOS (&&, ||, !)
    int f=7;
    float g=5.5F;
    char h='w';

    boolean resp1= (f >= 6) && (h == 'w');
    boolean resp2= (f >= 6) || (h == 119) ;
    boolean resp3= (g < 11) && (f > 100) ;
    boolean resp4= (h != 'p') || ((f + g) <= 10);
    boolean resp5= f + g <= 10;
    boolean resp6= f >= 6 && h == 'w' ;
    boolean resp7= h != 'p' || f + g <= 10;

    //OPERADORES INCREMENRALES
    int i=1;
    int j=i++; //Incremento
    int k=i--; //Decremento

    //OPERADORES DE ASIGNACION
    int l=5;
    int m=7;
    int n=2;
    int o=2;
    float p=5.5F;
    float q=-3.25F;
    double respAsig1=l += 5 ;
    double respAsig2=p -= q ;
    double respAsig3=m *= (l-3)  ;
    double respAsig4=p /= 3  ;
    double respAsig5=l %= (m - 2)  ;
    double respAsig6=n *= -2 * (o + n) / 3  ;

}
