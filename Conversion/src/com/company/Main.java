package com.company;

public class Main {

    public static void main(String[] args) {
	//Conversion por ASIGNACION (=)
        int numero;
        double e;
        numero=20;
        e=numero; // Asigna un valor de tipo entero a una variable double

     // Conversion por METODOS
        String cadena="123";
        int x=Integer.parseInt(cadena);

        String cadena2="123.45f";
        float y=Float.parseFloat(cadena2);

        String cadena3="99.9999";
        double z=Double.parseDouble(cadena3);

    // Promocion aritmetica
        short s=9;
        int i=10;
        float f = 11.1f;
        double d=12.2;
        if (++s*i >=f/d)
            System.out.println(">>>>");
        else
            System.out.println("<<<<");


    }
}
