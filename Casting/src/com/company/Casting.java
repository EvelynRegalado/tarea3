package com.company;

/**
 * Created by Evelyn on 04/06/2017.
 */
public class Casting {
    double x;
    double y;

    public Casting(double x, double y) {
        this.x=x;
        this.y=y;
    }

    public String toString() {
        return "x=" + x +
                ", y=" + y;
    }

    public float resultado() {
        //Casting
        float resp=(float)x * (float)y;
        return resp;
    }
}
